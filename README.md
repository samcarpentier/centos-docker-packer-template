# centos-docker-packer-box

Packer box with Docker installed and configured from a CentOS 7 base image

## Requirements

* Packer
* Ansible
* VirtualBox

## Usage

* Run `packer build centos-docker-template.json`

## Output

* A box file named `centos-<version>-docker`
